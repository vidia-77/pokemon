import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../../model/pokemon';

@Injectable({
  providedIn: 'root'
})
export class PokemonListService {
 
  pokemonListe= environment.jsonUrl;



  constructor( private httpClient:HttpClient) { }

  getPokemon():Observable<Pokemon[]>{ 
    return this.httpClient.get<Pokemon[]>(this.pokemonListe);
  }
  
  getPokemonById(id: number): Observable<Pokemon>{
    return this.httpClient.get<Pokemon[]>(this.pokemonListe).pipe(
      map((data: Pokemon[]) => {
        return data.find((pokemon) => {
          return pokemon.id === id;});
      }));
  }


  findPokemonByName(name:string):Observable<Pokemon>{
    return this.httpClient.get<Pokemon[]>(this.pokemonListe).pipe(
      map((data: Pokemon[]) => {
        return data.find((pokemon) => {
          return pokemon.name === name;});
      }));
  }

}
