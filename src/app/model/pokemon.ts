export class Pokemon {
    id:number;
    name:string;
    type:string;
    base:{HP:number, Attack:number, Defense:number, "Sp. Attack":number, "Sp. Defense":number, Speed:number};
    image:Blob;
    description:string;
    
}
