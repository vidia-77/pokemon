import { Component, ElementRef, OnInit } from '@angular/core';
import { ActivatedRoute, Route } from '@angular/router';

import { Pokemon } from '../model/pokemon';
import { PokemonListService } from '../service/pokelist/pokemon-list.service';

import VanillaTilt from "vanilla-tilt";

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {


 infopokemon:Pokemon=new Pokemon();



  constructor( private pokemonService:PokemonListService, 
                private route:ActivatedRoute,
                private el : ElementRef) { }

  ngOnInit() {
    this.getPokemonById(+this.route.snapshot.paramMap.get('id'));
    this.searchPokemonByName();
    VanillaTilt.init(this.el.nativeElement.querySelector(".vanillaTilt") as any);

  
  }

  getPokemonById(id:number){
    this.pokemonService.getPokemonById(id).subscribe( (data:Pokemon) => {
      this.infopokemon = data;
      console.log(this.infopokemon);
  } );  }

  searchPokemonByName()
  {
      const search:string = this.route.snapshot.paramMap.get('id');
      this.pokemonService.findPokemonByName(search).subscribe( (data => {this.infopokemon = data }));
  }





}







 






