import { Component,  OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from '../model/pokemon';
import { PokemonListService } from '../service/pokelist/pokemon-list.service';


@Component({
  selector: 'app-yellow-version',
  templateUrl: './yellow-version.page.html',
  styleUrls: ['./yellow-version.page.scss'],
})
export class YellowVersionPage implements OnInit {

 pokemon:Pokemon[];


  constructor( private pokemmonService:PokemonListService,
                public router:Router) {}

  ngOnInit(): void {
   this.getPokemon();
  }

  getPokemon(){
    this.pokemmonService.getPokemon().subscribe( (data:Pokemon[]) => {
      this.pokemon = data;
      //console.log(JSON.stringify(data));
    } ); 
  }

  getPokemonId(pokemons:Pokemon){
    this.router.navigateByUrl("details/"+pokemons.id).then((response)=>{console.log(response)}).catch((error)=>{console.log(error)});
    //console.log(pokemons);
  }

 


}
