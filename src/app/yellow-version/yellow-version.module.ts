import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { YellowVersionPageRoutingModule } from './yellow-version-routing.module';

import { YellowVersionPage } from './yellow-version.page';
import { AppModule } from "../app.module";

@NgModule({
    declarations: [YellowVersionPage],
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        YellowVersionPageRoutingModule
    ]
})
export class YellowVersionPageModule {}
