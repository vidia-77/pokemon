import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { YellowVersionPage } from './yellow-version.page';

const routes: Routes = [
  {
    path: '',
    component: YellowVersionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class YellowVersionPageRoutingModule {}
