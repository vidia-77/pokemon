import { Component,  OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from '../model/pokemon';
import { PokemonListService } from '../service/pokelist/pokemon-list.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  pokemonList:Pokemon[];


  constructor(public router:Router, private pokemonService:PokemonListService) {}

  ngOnInit(): void {
    this.getpokemonList();
  }

  findPokemonByName(value:any){
    console.log(value);
    this.router.navigateByUrl(`/details/${value}`)//.then((response)=>{console.log(response)}).catch((error)=>{console.log(error)});

  }

  getpokemonList(){
      this.pokemonService.getPokemon().subscribe( (data:Pokemon[]) => {
          this.pokemonList = data;
        })
    }

  }




